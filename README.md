# Git Love You

Parce que même si vous en doutiez, Git vous aime. Il vous adore et vous vous devez de le lui rendre.

![alt text](images/Git-Logo-1788C.png)

# Sommaire

## 01_Forker_un_depot_GitLab
*Apprenez à "***forker***" un dépôt GitLab d'une autre personne pour vous-même afin de pouvoir le modifier*  
**Tutoriel pour les personnes souhaitant reprendre de zéro**

## 02_Depuis_un_dossier_local
*Apprenez à mettre sur GitLab un répertoire local de votre ordinateur*  
**Tutoriel pour les personnes maîtrisant les bases de Git**

## 03_Git_pour_Windows
*Apprenez à installer Git sous Windows pour vous amuser sous n'importe quel OS*  
**Tutoriel pour les personnes qui doivent travailler sous Windows**

## 04_Mettre_a_jour
*Apprenez à mettre à jour un dépôt Git*  
**Tutoriel pour les gens qui aiment mettre à jour**

## 05_Commandes_Git
*Une liste des commandes Git et quelques astuces*  
**Tutoriel pour les gens qui ont tout compris et veulent juste une petite fiche de rappel**

## 06_Revenir_en_arriere
*Retrouvez votre code qui fonctionnait bien avant vos modifications*  
**Tutoriel pour les archivistes**

#### Pour apprendre les joies du ***merge*** et de la création de ***branchs***, n'hésitez à aller voir [les tutoriels de Nora](https://gitlab.com/Norahenn/merge-is-not-a-mystical-creature)

---

Git c'est la vie <3

C'est fini... *pour le moment...* #rire-maléfique